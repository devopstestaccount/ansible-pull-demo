Execute on remote machine
```
apt install git ansible -y
ansible-pull -o -C master -d /var/projects/ansible-pull-update -i /var/projects/ansible-pull-update/inventory -U https://gitlab.com/devopstestaccount/ansible-pull-demo.git | tee -a /var/log/ansible-pull-update.log
```
